$Id: $

Site Stage README

CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration
  * Usage
  * Known issues.


INTRODUCTION
------------


INSTALLATION
------------
At admin/build/modules enable the Site Stage module.

CONFIGURATION
-------------
1. Enable the permission "administer site staging" at admin/user/permissions.
 
2. Configure the node export module at admin/settings/node_export to have the following settings on each content type:
    "URL path" Not Checked
    
    

USAGE
-----


KNOWN ISSUES
------------