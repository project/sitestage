<?php

// $Id$

/**
 * @file
 * Admin page callback file for the site_stage module.
 */

/**
 * Admin page callback.
 */
function site_stage_admin($callback_arg = '') {
  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

  switch ($op) {
    case t('Create new environment'):
    case 'create':
      $build['new_environment'] = drupal_get_form('user_register_form');
      break;
    default:
      if (!empty($_POST['accounts']) && isset($_POST['operation']) && ($_POST['operation'] == 'cancel')) {
        $build['stage_multiple_disable_confirm'] = drupal_get_form('site_stage_multiple_disable_confirm');
      }
      else {
        $build['environment'] = drupal_get_form('site_stage_admin_form');
      }
  }
  return $build;
}

/**
 * Form builder; Site stage administration page.
 *
 * @ingroup forms
 * @see site_stage_admin_form_validate()
 * @see site_stage_admin_form_submit()
 */
function site_stage_admin_form() {

  $header = array(
    'name' => array('data'        => t('Environment Name'), 'field' => 'se.name'),
    'conn' => array('data'        => t('SSH Connection String')),
    'remote' => array('data'      => t('Remote Folder')),
    'main' => array('data'        => t('Main Server?')),
    'status' => array('data'      => t('Status'), 'field' => 'se.status'),
    'created' => array('data'     => t('Created')),
    'changed' => array('data'     => t('Changed')),
    'operations' => array('data'  => t('Operations')),
  );
  
  //  $query = db_select('stage_environment', 'se');
  //  $query
  //    ->fields('se')
  //    ->condition('sid', $sid)
  //    ->limit(1);
  //
  //  $result = $query->execute();
  
  $query = db_select('stage_environment', 'se');
  // $query->condition('u.uid', 0, '<>');
  // user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(se.sid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('se', array('sid', 'name', 'conn', 'remote', 'status', 'main', 'created', 'changed'))
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array();
  foreach (module_invoke_all('stage_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'unblock',
  );
  $options = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $destination = drupal_get_destination();

  $status = array(t('disabled'), t('enabled'));
  $main = array(t('no'), t('yes'));
  $accounts = array();
  foreach ($result as $stage) {
    $options[$stage->sid] = array(
      'name' => theme('stage_name', array('stage' => $stage)),
      'status' => $status[$stage->status],
      'main' => $main[$stage->main],
      'conn' => $stage->conn,
      'remote' => $stage->remote,
      'created' => format_interval(REQUEST_TIME - $stage->created),
      'changed' => format_interval(REQUEST_TIME - $stage->changed),
      'operations' => array('data' => array('#type' => 'link', '#title' => t('edit'), '#href' => "admin/stage/$stage->sid/edit", '#options' => array('query' => $destination))),
    );
  }

  $form['accounts'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No stages available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

function site_stage_admin_form_validate($form, &$form_state) {
  
}

function site_stage_admin_form_submit($form, &$form_state) {
}

/**
 * Site Stage Form
 */
function site_stage_form($form, &$form_state, $stage = NULL) {
  if (is_null($stage)) {
    $stage = new stdClass();
  }

  if (!isset($form_state['stage'])) {
    $form_state['stage'] = $stage;
  }
  else {
    $stage = $form_state['stage'];
  }
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Environment Name'),
    '#size' => 64,
  );
  $form['conn'] = array(
    '#type' => 'textfield',
    '#title' => t('SSH Connection String'),
    '#size' => 64,
  );
  $form['remote'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote Path'),
    '#size' => 64,
  );
  $form['status'] = array(
    '#type' => 'textfield',
    '#title' => t('Status'),
    '#size' => 64,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  
  return $form;
}


function site_stage_form_validate($form, &$form_state) {
  
}

function site_stage_form_submit($form, &$form_state) {
  $stage = $form_state['stage'];

  form_state_values_clean($form_state);

  $stage_unchanged = clone $stage;

  $edit = $form_state['values'];

  $stage = site_stage_save($stage_unchanged, $edit);
  $form_state['values']['sid'] = $stage->sid;

  drupal_set_message(t('The changes have been saved.'));  
}

/**
 * Saves the site stage record.
 */
function site_stage_save($stage, $edit) {
  $transaction = db_transaction();
  try {
    // Load the stored entity, if any.
    if (!empty($stage->sid)) {
      $stage->original = site_stage_load($stage->sid);
      var_dump($stage->original);
    }

    if (empty($stage)) {
      $stage = new stdClass();
    }
    if (!isset($stage->is_new)) {
      $stage->is_new = empty($stage->sid);
    }

    foreach ($edit as $key => $value) {
      $stage->$key = $value;
    }
    
    var_dump($stage);
    die();
    
    if (is_object($stage) && !$stage->is_new) {
      $success = drupal_write_record('stage_environment', $stage, 'sid');
      if ($success === FALSE) {
        // The query failed - better to abort the save than risk further
        // data loss.
        return FALSE;
      }
      // @TODO Handle the triggers for this environment.
    }
    else {
      if (empty($stage->sid)) {
        $stage->sid = db_next_id(db_query('SELECT MAX(sid) FROM {stage_environment}')->fetchField());
      }
      if (!isset($stage->created)) {
        $stage->created = REQUEST_TIME;
      }
      $success = drupal_write_record('stage_environment', $stage);
      if ($success === FALSE) {
        return FALSE;
      }
      // @TODO Handle the triggers for this environment.
    }
    // Clear internal properties.
    unset($stage->is_new);
    unset($stage->original);
    return $stage;
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('user', $e);
    throw $e;
  }
}

/**
 * Implements hook_stage_operations 
 */
function site_stage_stage_operations () {
  $operations = array();
  $operations['enable'] = array('label' => t('Enable'));
  $operations['disable'] = array('label' => t('Disable'));
  return $operations;
}

/**
 * Load a single record
 */
function site_stage_load($sid) {
  return db_query('SELECT * FROM {stage_environment} se WHERE se.sid = :sid', array(':sid' => $sid))->fetchObject();
}