<?php
// $Id$

/**
 * @file
 *   Drush support for site_stage.
 */

/**
 * Implements hook_drush_command().
 */
function site_stage_drush_command() {
  $items = array();

  $items['site-stage-export'] = array(
    'callback' => 'site_stage_drush_callback_export',
    'description' => "Export menus using menu-export.",
    'arguments' => array(
      'menus' => "A list of space-separated menu names (machine).",
    ),
    'options' => array(
    ),
    'aliases' => array(
      'menu-export',
    ),
    'examples' => array(
      'drush menu-export my-menu another-menu awesome-menu > export_file' =>
      'export my-menu, another-menu, and awesome-menu to file export_file',
      'drush menu-export' =>
      "export all menus to stdout",
    ),
  );
  $items['site-stage-import'] = array(
    'callback' => 'site_stage_drush_callback_import',
    'description' => "Import menus previously exported with node export menu.",
    'aliases' => array(
      'menu-import',
    ),
    'options' => array(
    ),
    'examples' => array(
      'drush menu-import < export_file' =>
      'Import menus from the file export_file.',
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function site_stage_drush_help($section) {
  // This is to prevent duplication of information from hook_drush_command().
  $commands = site_stage_drush_command();
}

/**
 * Drush command callback.
 *
 * export nodes.
 */
function site_stage_drush_callback_export() {
  $args = func_get_args();
  if (!empty($args)) {
    foreach ($args as $key => $menu_name) {
      $menus[$menu_name] = $menu_name;
    }
  }

  $result = _site_stage_generate_export_code($menus);

  if (!$result['result']) {
    // We have received an error message.
    $unknown_menus = implode(' ', $result['unknown_menus']);
    drush_set_error('DRUSH_NOT_COMPLETED', 'Export Failed: The following menus were not exportable (check spelling): ' . $unknown_menus);
  }
  else {
    drush_print_r($result['output']);
  }
}

/**
 * Drush command callback.
 *
 * Import nodes from data.
 */
function site_stage_drush_callback_import() {

  $menu_code = file_get_contents("php://stdin", "r");

  if (!empty($menu_code)) {
    $result = _site_stage_import_eval_code($menu_code);
    drush_print_r($result);
  }
}
