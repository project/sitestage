<?php
// $Id$

/**
 * @file
 * Stages code and content changes from one environment to multiple others.
 */

/**
 * Import bay path.
 */
define('STAGE_IMPORT_BAY', '/gfsshare/' . variable_get('site_name', t('Drupal')) . '/import');

/**
 * Export bay path
 */
define('STAGE_EXPORT_BAY', '/tmp/site_export/' . variable_get('site_name', t('Drupal')));

/**
 * Stage import backup path.
 */
define('STAGE_IMPORT_BACKUP', '/gfsshare/' . variable_get('site_name', t('Drupal')) . '/backups');

/**
 * Stage export backup path.
 */
define('STAGE_EXPORT_BACKUP', '/var/site_archive/' . variable_get('site_name', t('Drupal')));

/**
 * Implements hook_perm().
 */
function site_stage_perm() {
  return array('administer site stage', );
}

/**
 * Implements hook_init().
 */
function site_stage_init() {
  if (user_access('administer site stage')) {
    site_stage_check_folders();
  }
}

/**
 * Implementation of hook_menu()
 */
function site_stage_menu() {
  $items['admin/stage'] = array(
    'title' => 'Staging',
    'page callback' => 'site_stage_admin',
    'page arguments' => array('list'),
    'access arguments' => array('administer site stage'),
    'file' => 'site_stage.admin.inc',
  );
  $items['admin/stage/list'] = array(
    'title' => 'List Staging Environments',
    'description' => 'List the different staging environments for your site.',
    'access arguments' => array('administer site stage'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/stage/add'] = array(
    'title' => 'Add a Stage',
    'description' => 'Add a stage.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_stage_form'),
    'access arguments' => array('administer site stage'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'site_stage.admin.inc',
  );
  $items['admin/stage/%sid/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit stage.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_stage_form', 2),
    'access arguments' => array('administer site stage'),
    'file' => 'site_stage.admin.inc',
  );
  return $items;
}

/**
 * Makes sure all required folders are writeable.
 */
function site_stage_check_folders($reset = FALSE) {
  $folder_check = &drupal_static(__FUNCTION__, NULL, $reset);
  
  if (!isset($folder_check)) {
    if ($cache = cache_get('folder_check')) {
      $folder_check = $cache->data;
      if ($folder_check === TRUE) {
        return $folder_check;
      }
    }
    
    if ($folder_check !== TRUE) {
      $folders = array(
        'stage_import_bay'    => variable_get('stage_import_bay',     STAGE_IMPORT_BAY),
        'stage_export_bay'    => variable_get('stage_export_bay',     STAGE_EXPORT_BAY),
        'stage_import_backup' => variable_get('stage_import_backup',  STAGE_IMPORT_BACKUP),
        'stage_export_backup' => variable_get('stage_export_backup',  STAGE_EXPORT_BACKUP),
      );
      foreach ($folders as $key => $path) {
        if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
          $folder_check = ($folder_check === FALSE ? FALSE : TRUE);
          variable_set($key, $path);
          if (user_access('administer site stage')) {
            drupal_set_message($message = t('The %name folder %path is writable.', array('%name' => $key, '%path' => $path)), $type = 'status');
          }
        }
        else {
          $folder_check = FALSE;
          if (user_access('administer site stage')) {
            drupal_set_message($message = t('Cannot access %name folder %path. Please check permissions.', array('%name' => $key, '%path' => $path)), $type = 'error');
          }
          watchdog('site_stage', 'Cannot access %name folder %path. Please check permissions.', array('%name' => $key, '%path' => $path), WATCHDOG_CRITICAL);
        }
      }
      cache_set('folder_check', $folder_check, 'cache');
    }
  }
  return $folder_check;
}